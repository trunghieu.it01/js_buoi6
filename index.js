function timSoNhoNhat() {
  var sum = 0;
  for (var i = 1; sum < 10000; i++) {
    sum = sum + i;
  }
  i = i - 1;
  document.getElementById(
    "resultEx1"
  ).innerHTML = `<p>Số nguyên dương nhỏ nhất: ${i}</p>`;
}
function tinhTong() {
  var sum = 0;
  var soX = document.getElementById("txt-so-x").value * 1;
  var soN = document.getElementById("txt-so-n").value * 1;
  for (var i = 1; i <= soN; i++) {
    sum = sum + Math.pow(soX, i);
  }
  document.getElementById("resultEx2").innerHTML = `<p>Tổng: ${sum}`;
}
function tinhGiaiThua() {
  var sum = 1;
  var soN = document.getElementById("txt-so-n-ex3").value * 1;
  for (var i = 1; i <= soN; i++) {
    sum = sum * i;
  }
  document.getElementById("resultEx3").innerHTML = `<p>${soN}! = ${sum}`;
}
function taoThe() {
  var theDivCon = document.querySelectorAll(".che");
  var theDivTong = (document.getElementById("tong").style.display = "block");
  for (var i = 0; i <= 10; i++) {
    if ((i + 1) % 2 == 0) {
      theDivCon[i].style.background = "red";
    } else {
      theDivCon[i].style.background = "blue";
    }
  }
}
function kiemTraSoNguyenTo(n) {
  var flag = true;
  if (n < 2) {
    flag = false;
  } else if (n == 2) {
    flag = true;
  } else if (n % 2 == 0) {
    flag = false;
  } else {
    for (var i = 3; i <= Math.sqrt(n); i += 2) {
      if (n % i == 0) {
        flag = false;
        break;
      }
    }
  }

  return flag;
}
function inSoNguyenTo() {
  var number = document.getElementById("txt-so-n-ex5").value;
  var ketQua = "";
  for (var i = 1; i <= number; i++) {
    if (kiemTraSoNguyenTo(i)) {
      ketQua += i + " ";
    }
  }
  document.getElementById("resultEx5").innerHTML = ketQua;
}
